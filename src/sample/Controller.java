package sample;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox co1;
    public Button btn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<String> area = new ArrayList<>();
        area.add("紅區");
        area.add("藍區");
        area.add("黃區");
        area.add("綠區");
        co1.setItems(FXCollections.observableArrayList(area));    }

    public void btn1(ActionEvent actionEvent) {
        Stage stage=new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        Pane pane= null;
        try {
            pane = FXMLLoader.load(NewStage.class.getResource("x.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene=new Scene(pane,300,400);
        stage.setScene(scene);
        stage.showAndWait();
    }

    private class NewStage {
    }
}

